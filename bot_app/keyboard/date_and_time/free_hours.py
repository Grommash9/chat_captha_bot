import datetime

from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


def free_time(year, month, day):
    inline_kb_full = InlineKeyboardMarkup(row_width=4)
    now = datetime.datetime.now()
    hours_works_time = [hour for hour in range(8, 24)]
    for hour in hours_works_time:
        if now.year == int(year) and now.month == int(month) and now.day == int(day) and hour <= now.hour:
            inline_kb_full.insert(InlineKeyboardButton(f'🚫 {hour:02d}:00', callback_data='ignore'))
        else:
            inline_kb_full.insert(InlineKeyboardButton(f'🔹 {hour:02d}:00',
                                                       callback_data=f'time_{year}_{month}_{day}_{hour}'))
    inline_kb_full.add(InlineKeyboardButton("⬅️  Назад", callback_data='back-to-date'))
    return inline_kb_full
