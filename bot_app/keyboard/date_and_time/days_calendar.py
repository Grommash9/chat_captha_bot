import calendar
import datetime
import locale

from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup


def create_calendar(year=None, month=None):
    now = datetime.datetime.now()
    locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
    if year is None:
        year = now.year
    if month is None:
        month = now.month
    keyboard = []
    row_1 = []
    row_1.append(InlineKeyboardButton(calendar.month_name[month] + "  -  " + str(year), callback_data='ignore'))
    keyboard.append(row_1)
    weeks_days = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"]
    my_calendar = calendar.monthcalendar(year, month)
    for colum in range(7):
        row_2 = []
        row_2.append(InlineKeyboardButton(f'{weeks_days[colum]}', callback_data='ignore'))
        if month == now.month:
            for i_week in range(len(my_calendar)):
                if my_calendar[i_week][colum] == 0:
                    row_2.append(InlineKeyboardButton(' ', callback_data='ignore'))
                elif my_calendar[i_week][colum] < now.day:
                        row_2.append(InlineKeyboardButton(f'❗{my_calendar[i_week][colum]}', callback_data='ignore'))
                elif my_calendar[i_week][colum] == now.day:
                        row_2.append(InlineKeyboardButton(f'⚡{my_calendar[i_week][colum]}',
                                                     callback_data=f'day_{year}_{month}_{my_calendar[i_week][colum]}'))
                else:
                        row_2.append(InlineKeyboardButton(f'🔹{my_calendar[i_week][colum]}',
                                                    callback_data=f'day_{year}_{month}_{my_calendar[i_week][colum]}'))
            keyboard.append(row_2)
        else:
            for i_week in range(len(my_calendar)):
                if my_calendar[i_week][colum] == 0:
                    row_2.append(InlineKeyboardButton(' ', callback_data='ignore'))
                else:
                    row_2.append(InlineKeyboardButton(
                        f'🔹{my_calendar[i_week][colum]}', callback_data=f'day_{year}_{month}_{my_calendar[i_week][colum]}'))
            keyboard.append(row_2)
    row_3 = []
    if month != now.month:
        row_3.append(InlineKeyboardButton("<<", callback_data=f'prev-month_{year}_{month}_0'))
    if month != now.month+2:
        row_3.append(InlineKeyboardButton(">>", callback_data=f'next-month_{year}_{month}_0'))
    keyboard.append(row_3)
    row_4 = []
    keyboard.append(row_4)
    return InlineKeyboardMarkup(inline_keyboard=keyboard)