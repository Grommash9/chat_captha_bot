from aiogram.types import KeyboardButton, ReplyKeyboardMarkup


def admin_echo_button():
    full_kb = ReplyKeyboardMarkup(resize_keyboard=True)
    button1 = KeyboardButton('Создать рассылку')
    button2 = KeyboardButton('Настройка чатов ⚙')
    button3 = KeyboardButton('Статистика 📊')
    button4 = KeyboardButton('ON/OFF Bot')
    button5 = KeyboardButton('Настройки для новых чатов по умолчанию')
    full_kb.add(button1, button2)
    full_kb.row(button3, button4)
    full_kb.row(button5)
    return full_kb


def admin_cancel_button():
    full_kb = ReplyKeyboardMarkup(resize_keyboard=True)
    button1 = KeyboardButton('Отмена')
    full_kb.row(button1)
    return full_kb


def admin_choice_button():
    full_kb = ReplyKeyboardMarkup(resize_keyboard=True)
    button1 = KeyboardButton('Разослать')
    button2 = KeyboardButton('Отмена')
    full_kb.row(button1, button2)
    return full_kb


def change_country_button(list_country):
    full_kb = ReplyKeyboardMarkup(resize_keyboard=True)
    for country in list_country:
        full_kb.add(KeyboardButton(f'{country["country_name"]}'))
    full_kb.add(KeyboardButton('вернутся в основное меню'))
    return full_kb


def choice_country_dispatch(list_country):
    full_kb = ReplyKeyboardMarkup(resize_keyboard=True)
    for country in list_country:
        full_kb.add(KeyboardButton(f'Рассылка: {country["country_name"]}'))
    full_kb.add(KeyboardButton('вернутся в основное меню'))
    return full_kb


