from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.types.chat import Chat


def url_inline(dict_set):
    inline_kb_full = InlineKeyboardMarkup(row_width=True)
    count = 1
    for url in dict_set:
        inline_kb_full.add(InlineKeyboardButton(f'Подписаться на {count} канал', url=url['url']))
        count += 1
    return inline_kb_full


def all_chat_id(chat_list: list[Chat], country_id):
    inline_kb_full = InlineKeyboardMarkup(row_width=True)
    for chat in chat_list:
        inline_kb_full.add(InlineKeyboardButton(text=f'{chat.title}', callback_data=f'edit-channel_{chat.id}'))
    btn_1 = InlineKeyboardButton('+ добавить новый', callback_data=f'add-chat_{country_id}')
    inline_kb_full.add(btn_1)
    return inline_kb_full


def settings_button(chat_id, chat_settings):
    inline_btn_1 = InlineKeyboardButton('➕ добавить канал',
                                        callback_data=f'add-to-channel_{chat_id}')

    inline_btn_2 = InlineKeyboardButton('➖ удалить канал',
                                        callback_data=f'delete-from-channel_{chat_id}')

    if chat_settings['bot_enabled'] == 1:
        inline_btn_3 = InlineKeyboardButton('Состояние бота в чате: 🟢 включен',
                                            callback_data=f'bot-enabled_{chat_id}')
    else:
        inline_btn_3 = InlineKeyboardButton('Состояние бота в чате: 🔴 выключен',
                                            callback_data=f'bot-enabled_{chat_id}')

    if chat_settings['skipVerifiedUsers'] == 1:
        inline_btn_4 = InlineKeyboardButton('Верифицированных ранее пользователей: 🟢 пропускать ',
                                            callback_data=f'skip-Verified-Users_{chat_id}')
    else:
        inline_btn_4 = InlineKeyboardButton('Верифицированных ранее пользователей: 🔴 не пропускать',
                                             callback_data=f'skip-Verified-Users_{chat_id}')

    if chat_settings['kick_from_chat_on_fail'] == 1:
        inline_btn_5 = InlineKeyboardButton('🔴 Удалить с чата если не прошел CAPTCHA',
                                            callback_data=f'kick-from-chat-on-fail_{chat_id}')
    else:
        inline_btn_5 = InlineKeyboardButton('🟢 Не удалять с чата если не прошел CAPTCHA',
                                            callback_data=f'kick-from-chat-on-fail_{chat_id}')
    if chat_settings['ban_from_chat_on_fail'] == 1:
        inline_btn_6 = InlineKeyboardButton('🔴 Забанить в чате если не прошел CAPTCHA',
                                             callback_data=f'ban-from-chat-on-fail_{chat_id}')
    else:
        inline_btn_6 = InlineKeyboardButton('🟢 Не банить в чате если не прошел CAPTCHA',
                                             callback_data=f'ban-from-chat-on-fail_{chat_id}')

    inline_btn_7 = InlineKeyboardButton('🕒 Установить время на прохождение CAPTCHA',
                                        callback_data=f'time-to-pass-captcha_{chat_id}')

    inline_btn_8 = InlineKeyboardButton('Изменить приветственное сообщение после успешной CAPTCHA',
                                        callback_data=f'change-greetings-text-after-captcha_{chat_id}')

    inline_btn_9 = InlineKeyboardButton('Изменить приветственное сообщение в чате',
                                        callback_data=f'change-greetings-message-text_{chat_id}')

    inline_btn_10 = InlineKeyboardButton('Изменить приветственное фото',
                                         callback_data=f'change-greetings-photo_{chat_id}')
    inline_btn_11 = InlineKeyboardButton('⬅  ️назад', callback_data=f'back-to-channel-list_{chat_settings["country_id"]}')

    inline_kb_full = InlineKeyboardMarkup(row_width=1)
    inline_kb_full.row(inline_btn_1, inline_btn_2)
    inline_kb_full.add(inline_btn_3, inline_btn_4, inline_btn_5, inline_btn_6,
                       inline_btn_7, inline_btn_8, inline_btn_9, inline_btn_10, inline_btn_11)
    return inline_kb_full


def cancellation(chat_id):
    inline_kb_full = InlineKeyboardMarkup(row_width=1)
    inline_kb_full.add(InlineKeyboardButton(text='Отмена', callback_data=f'cancellation_{chat_id}'))
    return inline_kb_full


def cancel_chat(country_id):
    inline_kb_full = InlineKeyboardMarkup(row_width=1)
    inline_kb_full.add(InlineKeyboardButton(text='Отмена', callback_data=f'cancel-chat_{country_id}'))
    return inline_kb_full


def all_channel_id(chat_id, dict_channels):
    inline_channel_kb = InlineKeyboardMarkup(row_width=1)
    for channel_id, title in dict_channels.items():
        inline_channel_kb.insert(InlineKeyboardButton(f"{title[0]}",
                                 callback_data=f"delete-channel_{title[1]}_{channel_id}"))
    inline_channel_kb.insert(InlineKeyboardButton('Отмена', callback_data=f'cancellation_{chat_id}'))
    return inline_channel_kb


def change_del_channel(chat_id, channel_id):
    inline_kb_full = InlineKeyboardMarkup(row_width=2)
    inline_btn_1 = InlineKeyboardButton('Удалить', callback_data=f'sure-delete-channel_{chat_id}_{channel_id}')
    inline_btn_2 = InlineKeyboardButton('Отмена', callback_data=f'cancellation_{chat_id}')
    inline_kb_full.add(inline_btn_1, inline_btn_2)
    return inline_kb_full


def global_settings_button():
    inline_btn_1 = InlineKeyboardButton('🕒 Время на прохождение CAPTCHA',
                                        callback_data='default-time')
    inline_btn_2 = InlineKeyboardButton('Изменить приветственное сообщение в чате',
                                        callback_data='default-greetings-message')
    inline_btn_3 = InlineKeyboardButton('Изменить приветственное сообщение после успешной CAPTCHA',
                                        callback_data='default-text-after-captcha')
    inline_btn_4 = InlineKeyboardButton('Изменить приветственное фото',
                                        callback_data='default-greetings-photo')
    inline_kb_full = InlineKeyboardMarkup(row_width=1)
    inline_kb_full.add(inline_btn_1, inline_btn_2, inline_btn_3, inline_btn_4)
    return inline_kb_full


def settings_bot():
    inl_1 = InlineKeyboardButton('Перезапуск бота', callback_data='restart')
    inl_2 = InlineKeyboardButton('🟢 включить для 🇵🇱', callback_data='start_poland')
    inl_3 = InlineKeyboardButton('🔴 выключить для 🇵🇱', callback_data='stop_poland')
    inl_4 = InlineKeyboardButton('🟢 включить для 🇪🇸', callback_data='start_spain')
    inl_5 = InlineKeyboardButton('🔴 выключить для 🇪🇸', callback_data='stop_spain')

    inline_kb_full = InlineKeyboardMarkup(row_width=2)
    inline_kb_full.row(inl_1)
    inline_kb_full.add(inl_2, inl_3, inl_4, inl_5)
    return inline_kb_full

    # if bot_settings['bot_enabled'] == 1:
    #     inline_btn_3 = InlineKeyboardButton('Состояние бота в чате: 🟢 включен',
    #                                         callback_data=f'bot-enabled_{chat_id}')
    # else:
    #     inline_btn_3 = InlineKeyboardButton('Состояние бота в чате: 🔴 выключен',
    #                                         callback_data=f'bot-enabled_{chat_id}')
    #
    # if chat_settings['skipVerifiedUsers'] == 1:
    #     inline_btn_4 = InlineKeyboardButton('Верифицированных ранее пользователей: 🟢 пропускать ',
    #                                         callback_data=f'skip-Verified-Users_{chat_id}')
    # else:
    #     inline_btn_4 = InlineKeyboardButton('Верифицированных ранее пользователей: 🔴 не пропускать',
    #                                          callback_data=f'skip-Verified-Users_{chat_id}')
    #
    # if chat_settings['kick_from_chat_on_fail'] == 1:
    #     inline_btn_5 = InlineKeyboardButton('🔴 Удалить с чата если не прошел CAPTCHA',
    #                                         callback_data=f'kick-from-chat-on-fail_{chat_id}')
    # else:
    #     inline_btn_5 = InlineKeyboardButton('🟢 Не удалять с чата если не прошел CAPTCHA',
    #                                         callback_data=f'kick-from-chat-on-fail_{chat_id}')
    # if chat_settings['ban_from_chat_on_fail'] == 1:
    #     inline_btn_6 = InlineKeyboardButton('🔴 Забанить в чате если не прошел CAPTCHA',
    #                                          callback_data=f'ban-from-chat-on-fail_{chat_id}')
    # else:
    #     inline_btn_6 = InlineKeyboardButton('🟢 Не банить в чате если не прошел CAPTCHA',
    #                                          callback_data=f'ban-from-chat-on-fail_{chat_id}')
    #
    # inline_btn_7 = InlineKeyboardButton('🕒 Установить время на прохождение CAPTCHA',
    #                                     callback_data=f'time-to-pass-captcha_{chat_id}')
    #
    # inline_btn_8 = InlineKeyboardButton('Изменить приветственное сообщение после успешной CAPTCHA',
    #                                     callback_data=f'change-greetings-text-after-captcha_{chat_id}')
    #
    # inline_btn_9 = InlineKeyboardButton('Изменить приветственное сообщение в чате',
    #                                     callback_data=f'change-greetings-message-text_{chat_id}')
    #
    # inline_btn_10 = InlineKeyboardButton('Изменить приветственное фото',
    #                                      callback_data=f'change-greetings-photo_{chat_id}')
    # inline_btn_11 = InlineKeyboardButton('⬅  ️назад', callback_data=f'back-to-channel-list_{chat_settings["country_id"]}')
    #
    # inline_kb_full = InlineKeyboardMarkup(row_width=1)
    # inline_kb_full.row(inline_btn_1, inline_btn_2)
    # inline_kb_full.add(inline_btn_3, inline_btn_4, inline_btn_5, inline_btn_6,
    #                    inline_btn_7, inline_btn_8, inline_btn_9, inline_btn_10, inline_btn_11)
    # return inline_kb_full


def show_all_statistic():
    inline_kb_full = InlineKeyboardMarkup(row_width=1)
    inline_kb_full.add(InlineKeyboardButton('Dashboard  📊', url='https://datastudio.google.com/s/kNH1GzJ7lG0'))
    return inline_kb_full