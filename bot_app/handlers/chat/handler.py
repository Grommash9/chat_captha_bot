import json
import datetime
from zoneinfo import ZoneInfo

import aiogram.types
from aiogram.types import ChatMemberUpdated
from aiogram.utils.exceptions import BadRequest, MessageToDeleteNotFound
from apscheduler.jobstores.base import ConflictingIdError

import bot_app.db.users
from bot_app.db.captcha_log import new_note
from bot_app.db.members import new_note_members
from bot_app.keyboard.inline_markup import url_inline
from bot_app.misc import dp, bot, redis
from bot_app.misc import scheduler


async def ban_user(chat_id, user_id):
    await bot.ban_chat_member(chat_id=chat_id, user_id=user_id)
    await new_note(chat_id=chat_id, date=datetime.datetime.now().date(), ban=1)


async def kick_user(chat_id, user_id):
    await bot.kick_chat_member(chat_id=chat_id, user_id=user_id)
    await new_note(chat_id=chat_id, date=datetime.datetime.now().date(), kicked=1)


async def delete_message(chat_id, message_id):
    try:
        await bot.delete_message(chat_id=chat_id, message_id=message_id)
    except MessageToDeleteNotFound:
        pass


@dp.chat_member_handler()
async def chat_member_update(event: ChatMemberUpdated):

    if event.chat.type == aiogram.types.ChatType.CHANNEL:
        if event.new_chat_member.status == 'member':
            await new_note_members(channel_id=event.chat.id, channel_name=event.chat.title,
                                   date=event.date.date(), member=1)
        elif event.new_chat_member.status == 'left':
            await new_note_members(channel_id=event.chat.id, channel_name=event.chat.title,
                                   date=event.date.date(), left=1)
        return

    if event.new_chat_member.user.is_bot:
        return
    # await bot_app.db.chat_settings.create_chat(event.chat.id)
    chat_settings = await bot_app.db.chat_settings.get_chat_settings(event.chat.id)
    if chat_settings['bot_enabled'] != 1:
        return
    if chat_settings['skipVerifiedUsers'] != 0:
        chat_dict = await bot_app.db.chat_users.get_users_chats(event.new_chat_member.user.id)
        if chat_dict is not None:
            return

    if event.new_chat_member.status not in ['member', 'restricted']:
        return

    if event.old_chat_member.status == 'member' and event.new_chat_member.status == 'restricted':
        return

    if event.new_chat_member.status in ['left', 'kicked']:
        return

    is_member = True
    target_channel_id = await bot_app.db.chat_settings.get_target_channels(event.chat.id)
    for channel in target_channel_id:
        chat_member = await bot.get_chat_member(chat_id=channel['channel_id'],
                                                user_id=event.new_chat_member.user.id)
        if chat_member.status not in ['owner', 'member', 'creator', 'admin']:
            is_member = False
            break

    if is_member:
        return

    await new_note(event.chat.id, event.date.date(), start_check=1)
    await bot.restrict_chat_member(chat_id=event.chat.id,
                                   user_id=event.new_chat_member.user.id,
                                   can_send_messages=False)

    target_user = f'<a href="tg://user?id={event.new_chat_member.user.id}">{event.new_chat_member.user.first_name}</a>, '

    try:
        message_to_new_user = await bot.send_photo(event.chat.id,
                                                   chat_settings['greetings_message_photo'],
                                                   target_user + chat_settings['greetings_message_text'],
                                                   reply_markup=url_inline(target_channel_id))
    except BadRequest:
        message_to_new_user = await bot.send_message(event.chat.id,
                                                     target_user + chat_settings['greetings_message_text'],
                                                     reply_markup=url_inline(target_channel_id))

    del_time_mess = datetime.datetime.now(tz=ZoneInfo("Europe/Minsk")) + datetime.timedelta(
        seconds=chat_settings['time_to_pass_captcha'])

    try:
        scheduler.add_job(delete_message, 'date', run_date=del_time_mess,
                          args=(event.chat.id, message_to_new_user.message_id),
                          id=f'del_message_id_{message_to_new_user.message_id}_{event.new_chat_member.user.id}')
    except ConflictingIdError:
        pass
    ban_kick_time_user = datetime.datetime.now(tz=ZoneInfo("Europe/Minsk")) + datetime.timedelta(
        seconds=chat_settings['time_to_pass_captcha'])

    if chat_settings['ban_from_chat_on_fail']:
        try:
            scheduler.add_job(ban_user, 'date', run_date=ban_kick_time_user,
                              args=(event.chat.id, event.new_chat_member.user.id),
                              id=f'ban_{event.chat.id}_{event.new_chat_member.user.id}',
                              name=f'user_{event.new_chat_member.user.id}')
        except ConflictingIdError:
            pass

    elif chat_settings['kick_from_chat_on_fail']:
        try:
            scheduler.add_job(kick_user, 'date', run_date=ban_kick_time_user,
                              args=(event.chat.id, event.new_chat_member.user.id),
                              id=f'kick_{event.chat.id}_{event.new_chat_member.user.id}',
                              name=f'user_{event.new_chat_member.user.id}')
        except ConflictingIdError:
            pass

    await redis.set(f'user-verification_{event.new_chat_member.user.id}_{event.chat.id}', json.dumps(
        {'message_id': message_to_new_user.message_id, 'chat_id': event.chat.id}),
                    chat_settings['time_to_pass_captcha'])
