import json

from aiogram.types import ChatJoinRequest
from aiogram.utils.exceptions import BadRequest
from apscheduler.jobstores.base import JobLookupError

import bot_app.db.users
from bot_app.db.captcha_log import new_note
from bot_app.db.members import new_note_members
from bot_app.misc import dp, bot, redis
from bot_app.misc import scheduler


@dp.chat_join_request_handler()
async def new_join_request_process(event: ChatJoinRequest):
    await bot_app.db.users.create_user(event.from_user)
    # chat_data = await bot.get_chat(event.chat.id)
    # print('channel', event)
    # await new_note_members(channel_id=event.chat.id, channel_name=chat_data["title"],
    #                        date=event.date.date(), member=1)
    greet_mess = await bot.send_message(chat_id=event.from_user.id,
                                        text='Поздравляем, Ваш запрос на добавление в канал был одобрен.')
    await event.approve()

    await bot.delete_message(chat_id=event.from_user.id, message_id=greet_mess['message_id'])

    for keys in await redis.keys(f'user-verification_{event.from_user.id}*'):

        data = await redis.get(keys)

        dict_data = json.loads(data)
        chat_id = dict_data['chat_id']
        message_id = dict_data['message_id']

        target_channel_id = await bot_app.db.chat_settings.get_target_channels(chat_id)
        chat_settings = await bot_app.db.chat_settings.get_chat_settings(chat_id)
        chat_info = await bot.get_chat(chat_id)

        url_target_chat = chat_info["title"]
        for channel in target_channel_id:
            chat_member = await bot.get_chat_member(chat_id=channel['channel_id'], user_id=event.from_user.id)
            if chat_member.status in ['restricted', 'left', 'banned']:
                return

        for job in scheduler.get_jobs():
            if job.name == f'user_{event.from_user.id}':
                try:
                    scheduler.remove_job(job_id=f'ban_{chat_id}_{event.from_user.id}')
                except JobLookupError:
                    scheduler.remove_job(job_id=f'kick_{chat_id}_{event.from_user.id}')
                try:
                    scheduler.reschedule_job(job_id=f'del_message_id_{message_id}_{event.from_user.id}')
                except JobLookupError:
                    pass

        await bot.send_message(chat_id=event.from_user.id,
                               text=f"{event.from_user.first_name} теперь вы можете писать в чат: {url_target_chat}"
                                    f"\n\n\n{chat_settings['message_after_captcha']}")

        await bot.restrict_chat_member(chat_id=chat_id, user_id=event.from_user.id,
                                       can_send_messages=True,
                                       can_send_media_messages=True,
                                       can_send_other_messages=True,
                                       can_add_web_page_previews=True)

        await new_note(event.chat.id, event.date.date(), member=1)
        await bot_app.db.chat_users.create_chat_user(event.chat.id, event.from_user.id)
        await redis.delete(keys)
