import aiogram.types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import state
from aiogram.types import CallbackQuery, Message
from aiogram.utils.exceptions import ChatNotFound, BotKicked, MessageToDeleteNotFound

import bot_app.keyboard.reply_cancel_button
from bot_app import config
from bot_app.db.chat_settings import get_all_chat_id, get_chat_settings, get_all_chat_id_for_country, \
    get_target_channels, get_all_chat_settings
from bot_app.db.country import get_all_country
from bot_app.db.start_stop_bot import bot_disabled_for_country, bot_enabled_for_country
from bot_app.keyboard.inline_markup import settings_button, settings_bot
from bot_app.keyboard.inline_markup import all_chat_id
from bot_app.misc import dp, bot, redis
from bot_app.keyboard.reply_echo_admin_button import admin_echo_button, admin_cancel_button


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text='ON/OFF Bot')
async def on_off_bot(message: Message):
    await bot.send_message(message.from_user.id, 'Вы в меню управления ботом',
                           reply_markup=bot_app.keyboard.reply_cancel_button.back_to_main_menu())
    await bot.send_message(message.from_user.id, 'Выберете команду:',
                           reply_markup=settings_bot())


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='stop_poland')
async def stop_bot_poland(call: CallbackQuery):
    await call.answer()
    await bot_disabled_for_country(1)
    await call.message.edit_text('Для всех чатов по стране: Польша бот выключен!')
    await call.message.answer('Выберете команду:',
                              reply_markup=settings_bot())


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='start_poland')
async def start_bot_poland(call: CallbackQuery):
    await call.answer()
    await bot_enabled_for_country(1)
    await call.message.edit_text('Для всех чатов по стране: Польша бот включен!')
    await call.message.answer('Выберете команду:',
                              reply_markup=settings_bot())


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='stop_spain')
async def stop_bot_spain(call: CallbackQuery):
    await call.answer()
    await bot_disabled_for_country(2)
    await call.message.edit_text('Для всех чатов по стране: Испания бот выключен!')
    await call.message.answer('Выберете команду:',
                              reply_markup=settings_bot())


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='start_spain')
async def start_bot_spain(call: CallbackQuery):
    await call.answer()
    await bot_enabled_for_country(2)
    await call.message.edit_text('Для всех чатов по стране: Испания бот включен!')
    await call.message.answer('Выберете команду:',
                              reply_markup=settings_bot())


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='restart')
async def start_bot_spain(call: CallbackQuery):
    await call.answer()
    await call.message.edit_text('Бот успешно перезапущен!')
    await call.message.answer('Выберете команду:',
                              reply_markup=settings_bot())


@dp.message_handler(aiogram.filters.ChatTypeFilter(aiogram.types.ChatType.PRIVATE))
async def eny_text_messages(message: Message):
    if message.from_user.id not in config.ADMINS_ID:
        await bot.send_message(chat_id=message.from_user.id, text='Бот успешно перезапущен')
        return
    await bot.send_message(chat_id=message.from_user.id,
                           text='Ошибочный ввод. Возможно вы имели ввиду: /start или /admin')