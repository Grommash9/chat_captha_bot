import aiogram.types
from aiogram.types import CallbackQuery
from aiogram.utils.exceptions import ChatNotFound, BotKicked, MessageToDeleteNotFound

import bot_app.db.country
from bot_app import config
from bot_app.db.chat_settings import get_chat_settings, switch_bot_enabled, switch_skip_verified_users, \
    switch_kick_from_chat_on_fail, get_all_chat_id_for_country, switch_ban_from_chat_on_fail
from bot_app.handlers.admin.admin_echo import create_chat_info
from bot_app.keyboard.inline_markup import all_chat_id
from bot_app.keyboard.inline_markup import settings_button
from bot_app.keyboard.reply_cancel_button import back_to_main_menu
from bot_app.misc import dp, bot


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='back-to-channel-list')
async def settings_menu(call: CallbackQuery):
    await call.answer()
    try:
        await bot.delete_message(call.from_user.id, call.message.message_id-1)
    except MessageToDeleteNotFound:
        pass
    country_id = int(call.data.split('_')[1])
    all_chat_dict = await get_all_chat_id_for_country(country_id)
    chat_data_list = []
    for chat_id in all_chat_dict:
        try:
            chat_data = await bot.get_chat(chat_id['chat_id'])
            chat_data_list.append(chat_data)
        except (ChatNotFound, BotKicked):
            pass
    data = await bot_app.db.country.get_country_name(country_id)
    country_name = data['country_name']
    await call.message.delete()
    await bot.send_message(call.from_user.id, f'Возврат в меню выбора чатов по стране: <b>{country_name}</b>',
                           reply_markup=back_to_main_menu())
    await call.message.answer('Доступные чаты для настройки:', reply_markup=all_chat_id(chat_data_list, country_id))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='cancellation')
async def settings_menu(call: CallbackQuery):
    await call.answer()
    chat_id = call.data.split('_')[1]
    text, chat_settings = await create_chat_info(chat_id)
    await call.message.edit_text(text)
    await call.message.answer(f'<strong>Меню настроек чата:</strong>',
                              reply_markup=settings_button(chat_id, chat_settings))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='skip-Verified-Users')
async def settings_menu(call: CallbackQuery):

    await call.answer()
    chat_id = int(call.data.split('_')[1])
    await switch_skip_verified_users(chat_id)
    chat_settings = await get_chat_settings(chat_id)
    await call.message.edit_reply_markup(reply_markup=settings_button(chat_id, chat_settings))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='bot-enabled')
async def settings_menu(call: CallbackQuery):
    await call.answer()
    chat_id = int(call.data.split('_')[1])
    await switch_bot_enabled(chat_id)
    chat_settings = await get_chat_settings(chat_id)
    await call.message.edit_reply_markup(reply_markup=settings_button(chat_id, chat_settings))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='kick-from-chat-on-fail')
async def settings_menu(call: CallbackQuery):

    await call.answer()
    chat_id = int(call.data.split('_')[1])
    await switch_kick_from_chat_on_fail(chat_id)
    chat_settings = await get_chat_settings(chat_id)
    await call.message.edit_reply_markup(reply_markup=settings_button(chat_id, chat_settings))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='ban-from-chat-on-fail')
async def settings_menu(call: CallbackQuery):

    await call.answer()
    chat_id = int(call.data.split('_')[1])
    await switch_ban_from_chat_on_fail(chat_id)
    chat_settings = await get_chat_settings(chat_id)
    await call.message.edit_reply_markup(reply_markup=settings_button(chat_id, chat_settings))


