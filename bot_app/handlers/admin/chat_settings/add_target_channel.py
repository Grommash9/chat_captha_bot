import aiogram.types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import CallbackQuery, Message
from aiogram.utils.exceptions import ChatNotFound

import bot_app.db.chat_settings
from bot_app import config
from bot_app.db.chat_settings import get_chat_settings
from bot_app.handlers.admin.admin_echo import create_chat_info
from bot_app.keyboard.inline_markup import cancellation, settings_button
from bot_app.keyboard.reply_cancel_button import cancel_btn, back_to_main_menu
from bot_app.keyboard.reply_echo_admin_button import admin_cancel_button, admin_echo_button
from bot_app.misc import dp, bot


class NewChannel(StatesGroup):
    new_channel = State()


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='add-to-channel')
async def settings_menu(call: CallbackQuery, state: FSMContext):
    await call.answer()
    chat_id = call.data.split('_')[1]
    await call.message.delete()
    await bot.send_message(call.from_user.id, 'Добавление канала', reply_markup=cancel_btn())
    await call.message.answer('Введите id канала:')

    # await bot.delete_message(call.from_user.id, call.message.message_id-1)
    # await call.message.edit_text(text='Введите id канала:', reply_markup=cancellation(chat_id))
    await NewChannel.new_channel.set()
    await state.set_data({'chat_id': chat_id})


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='Отмена',
                    state=NewChannel.new_channel)
async def cancel_new_channel(message: Message, state: FSMContext):
    data = await state.get_data()
    chat_id = data['chat_id']
    await state.finish()
    await bot.delete_message(message.from_user.id, message.message_id)
    await bot.send_message(message.from_user.id, 'Добавление канала отменено.',
                           reply_markup=back_to_main_menu())
    text, chat_settings = await create_chat_info(chat_id)
    await bot.send_message(message.from_user.id, f'{text}\n\n<strong>Меню настроек чата:</strong>',
                           reply_markup=settings_button(chat_id, chat_settings))


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), state=NewChannel.new_channel)
async def get_new_channel(message: Message, state: FSMContext):
    data = await state.get_data()
    chat_id = data['chat_id']
    chat_settings = await get_chat_settings(data['chat_id'])
    try:
        new_channel = int(message.text)
    except ValueError:
        await message.answer('Ошибка ввода. Введите id канала в формате "-**************"')
        return
    try:
        await bot.get_chat_administrators(new_channel)
        new_channel_info = await bot.get_chat(new_channel)

        channel_invite_url = await bot.create_chat_invite_link(new_channel, creates_join_request=True)

        await bot_app.db.chat_settings.add_target_channel(chat_id, new_channel_info['id'],
                                                          channel_invite_url['invite_link'])
        await state.finish()
        text, chat_settings = await create_chat_info(chat_id)
        await bot.delete_message(message.from_user.id, message.message_id - 1)
        await bot.delete_message(message.from_user.id, message.message_id - 2)
        await bot.send_message(message.from_user.id, 'Канал успешно добавлен')
        await bot.send_message(message.from_user.id, f'{text}\n\n<strong>Меню настроек чата:</strong>',
                               reply_markup=settings_button(chat_id, chat_settings))
    except ChatNotFound:
        await state.finish()
        await message.answer('Действие невозможно. Добавьте бот в администраторы канала')
        return
        # await bot.send_message(message.from_user.id, text='Действие невозможно. Добавьте бот в администраторы канала',
        #                        reply_markup=settings_button(chat_id, chat_settings))

