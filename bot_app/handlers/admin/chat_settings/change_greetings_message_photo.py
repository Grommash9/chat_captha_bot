import aiogram.types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import CallbackQuery, Message

from bot_app import config
from bot_app.db.chat_settings import update_greetings_message_text, update_greetings_message_photo, get_chat_settings
from bot_app.handlers.admin.admin_echo import create_chat_info
from bot_app.keyboard.inline_markup import settings_button
from bot_app.keyboard.reply_cancel_button import cancel_btn, back_to_main_menu
from bot_app.misc import dp, bot


class NewPhotoLink(StatesGroup):
    photo_link = State()


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='change-greetings-photo')
async def edit_photo(call: CallbackQuery, state: FSMContext):
    await call.answer()
    chat_id = call.data.split('_')[1]
    await call.message.delete()
    await bot.send_message(call.message.chat.id, 'Изменение приветственного фото',
                           reply_markup=cancel_btn())
    await call.message.answer('Загрузите новое фото:')
    await NewPhotoLink.photo_link.set()
    await state.set_data({'chat_id': chat_id})


@dp.message_handler(text='Отмена', state=NewPhotoLink.photo_link)
async def cancel_new_photo(message: Message, state: FSMContext):
    data = await state.get_data()
    chat_id = data['chat_id']
    await state.finish()
    await bot.delete_message(message.from_user.id, message.message_id)
    await bot.delete_message(message.from_user.id, message.message_id - 1)
    await bot.send_message(message.from_user.id, 'Изменение приветственного фото отменено.',
                           reply_markup=back_to_main_menu())
    text, chat_settings = await create_chat_info(chat_id)
    await bot.send_message(message.from_user.id, f'{text}\n\n<strong>Меню настроек чата:</strong>',
                           reply_markup=settings_button(chat_id, chat_settings))


@dp.message_handler(state=NewPhotoLink.photo_link, content_types=aiogram.types.ContentType.PHOTO)
async def get_new_photo(message: Message, state: FSMContext):
    photo_link = message.photo[-1].file_id
    data = await state.get_data()
    chat_id = data['chat_id']
    await state.finish()
    await update_greetings_message_photo(photo_id=photo_link, chat_id=chat_id)
    await bot.delete_message(message.from_user.id, message.message_id - 1)
    text, chat_settings = await create_chat_info(chat_id)
    await bot.send_message(message.chat.id, f'Приветственное фото успешно изменено',
                           reply_markup=back_to_main_menu())
    await bot.send_message(message.from_user.id, f'{text}\n\n<strong>Меню настроек чата:</strong>',
                           reply_markup=settings_button(chat_id, chat_settings))

