import aiogram.types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import CallbackQuery, Message
from aiogram.utils.exceptions import ChatNotFound, BotKicked, MigrateToChat

from bot_app import config
from bot_app.db.chat_settings import get_chat_settings, get_all_chat_id_for_country, create_chat
from bot_app.keyboard.inline_markup import cancellation, settings_button, cancel_chat, all_chat_id
from bot_app.keyboard.reply_cancel_button import back_to_main_menu, cancel_btn
from bot_app.keyboard.reply_echo_admin_button import admin_cancel_button, admin_echo_button
from bot_app.misc import dp, bot


class NewChat(StatesGroup):
    new_chat = State()


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='add-chat')
async def add_chat(call: CallbackQuery, state: FSMContext):
    await call.answer()
    country_id = call.data.split('_')[1]
    await call.message.delete()
    await bot.send_message(call.from_user.id, 'Добавление чата', reply_markup=cancel_btn())
    await call.message.answer('Введите id чата:')
    await NewChat.new_chat.set()
    await state.set_data({'country_id': country_id})


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='Отмена',
                    state=NewChat.new_chat)
async def cancel_new_chat(message: Message, state: FSMContext):
    data = await state.get_data()
    country_id = data['country_id']
    await state.finish()
    all_chat_dict = await get_all_chat_id_for_country(country_id)
    chat_data_list = []
    for chat_id in all_chat_dict:
        try:
            chat_data = await bot.get_chat(chat_id['chat_id'])
            chat_data_list.append(chat_data)
        except (ChatNotFound, BotKicked):
            pass
    await bot.delete_message(message.from_user.id, message.message_id)
    await bot.send_message(message.from_user.id, 'Добавление чата отменено.',
                           reply_markup=back_to_main_menu())
    await bot.send_message(message.from_user.id, 'Доступные чаты для настройки:',
                           reply_markup=all_chat_id(chat_data_list, country_id))


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), state=NewChat.new_chat)
async def get_new_chat(message: Message, state: FSMContext):
    data = await state.get_data()
    country_id = data['country_id']
    try:
        new_chat = int(message.text)
    except ValueError:
        await message.answer('Ошибка ввода. Введите id чата в формате "-**************"')
        return
    try:
        await state.finish()
        await bot.get_chat_administrators(new_chat)
        await create_chat(country_id, new_chat)
        all_chat_dict = await get_all_chat_id_for_country(country_id)
        chat_data_list = []
        for chat_id in all_chat_dict:
            try:
                chat_data = await bot.get_chat(chat_id['chat_id'])
                chat_data_list.append(chat_data)
            except (ChatNotFound, BotKicked):
                pass
        await bot.send_message(message.from_user.id, text='Чат успешно добавлен',
                               reply_markup=back_to_main_menu())
        await bot.send_message(message.from_user.id, 'Доступные чаты для настройки:',
                               reply_markup=all_chat_id(chat_data_list, country_id))
    except ChatNotFound:
        await state.finish()
        await bot.send_message(message.from_user.id,
                               'Действие невозможно. Добавьте бот в администраторы чата и попробуйте снова',
                               reply_markup=admin_echo_button())
    except MigrateToChat:
        await state.finish()
        await bot.send_message(message.from_user.id, text='Действие невозможно. Чат не публичный',
                               reply_markup=admin_echo_button())

