import aiogram.types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import CallbackQuery, Message

from bot_app import config
from bot_app.db.chat_settings import update_time_to_pass_captcha, get_chat_settings
from bot_app.handlers.admin.admin_echo import create_chat_info
from bot_app.keyboard.inline_markup import settings_button
from bot_app.keyboard.reply_cancel_button import cancel_btn, back_to_main_menu
from bot_app.misc import dp, bot


class SetTime(StatesGroup):
    new_time = State()


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='time-to-pass-captcha')
async def edit_time_captcha(call: CallbackQuery, state: FSMContext):
    await call.answer()
    chat_id = call.data.split('_')[1]
    await call.message.delete()
    await bot.send_message(call.from_user.id, 'Изменение времени для прохождения CAPTCHA', reply_markup=cancel_btn())
    await call.message.answer('Введите новое время от 0 до 999:')
    await SetTime.new_time.set()
    await state.set_data({'chat_id': chat_id})


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='Отмена',
                    state=SetTime.new_time)
async def cancel_new_time(message: Message, state: FSMContext):
    data_state = await state.get_data()
    chat_id = data_state['chat_id']
    await state.finish()
    await bot.delete_message(message.from_user.id, message.message_id)
    await bot.delete_message(message.from_user.id, message.message_id - 1)
    await bot.send_message(message.from_user.id, 'Изменение времени отменено.',
                           reply_markup=back_to_main_menu())
    text, chat_settings = await create_chat_info(chat_id)
    await bot.send_message(message.from_user.id, f'{text}\n\n<strong>Меню настроек чата:</strong>',
                           reply_markup=settings_button(chat_id, chat_settings))


@dp.message_handler(state=SetTime.new_time, regexp='^[0-9]$|^[1-9][0-9]$|^[1-9][0-9][0-9]$')
async def get_new_time_captcha(message: Message, state: FSMContext):
    new_time = message.text
    data = await state.get_data()
    chat_id = data['chat_id']
    await state.finish()
    await update_time_to_pass_captcha(new_time, chat_id)
    text, chat_settings = await create_chat_info(chat_id)
    await bot.delete_message(message.from_user.id, message.message_id - 1)
    await bot.delete_message(message.from_user.id, message.message_id - 2)
    await bot.send_message(message.from_user.id, 'Время для прохождения CAPTCHA успешно изменено',
                           reply_markup=back_to_main_menu())
    await bot.send_message(message.from_user.id, f'{text}\n\n<strong>Меню настроек чата:</strong>',
                           reply_markup=settings_button(chat_id, chat_settings))

