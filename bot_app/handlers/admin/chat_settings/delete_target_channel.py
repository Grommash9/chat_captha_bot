import aiogram.types
from aiogram.types import CallbackQuery
from aiogram.utils.exceptions import ChatNotFound

import bot_app.db.chat_settings
from bot_app import config
from bot_app.db.chat_settings import get_chat_settings
from bot_app.handlers.admin.admin_echo import create_chat_info
from bot_app.keyboard.inline_markup import settings_button, all_channel_id, change_del_channel
from bot_app.misc import dp, bot


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='delete-from-channel')
async def del_channel_menu(call: CallbackQuery):
    await call.answer()
    chat_id = call.data.split('_')[1]
    data_channels = await bot_app.db.chat_settings.get_target_channels(int(chat_id))
    print(data_channels)
    dict_inline_markup = dict()
    for i_channel in data_channels:
        channel_id = i_channel['channel_id']
        if channel_id:
            try:
                channel_info = await bot.get_chat(channel_id)
            except ChatNotFound:
                pass
            else:
                dict_inline_markup[channel_id] = [channel_info['title'], chat_id]
        else:
            pass
    await call.message.edit_text(text='Выберите канал для удаления:',
                                 reply_markup=all_channel_id(chat_id, dict_inline_markup))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='delete-channel')
async def del_channel_question(call: CallbackQuery):
    await call.answer()
    chat_id = call.data.split('_')[1]
    channel_id = call.data.split('_')[2]
    await call.message.edit_text('Вы действительно хотите удалить канал?',
                                 reply_markup=change_del_channel(chat_id, channel_id))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='sure-delete-channel')
async def del_channel(call: CallbackQuery):
    await call.answer()
    chat_id = call.data.split('_')[1]
    channel_id = call.data.split('_')[2]
    await bot_app.db.chat_settings.delete_target_channel(int(chat_id), int(channel_id))
    # await bot.delete_message(message.from_user.id, message.message_id - 1)
    # await bot.delete_message(message.from_user.id, message.message_id - 2)
    text, chat_settings = await create_chat_info(chat_id)
    await call.message.edit_text(f'{text}\n\n<strong>Меню настроек чата:</strong>',
                                 reply_markup=settings_button(chat_id, chat_settings))

    # chat_settings = await get_chat_settings(int(chat_id))
    # await call.message.edit_text(text='Канал успешно удален',
    #                              reply_markup=settings_button(chat_id, chat_settings))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='cancellation_')
async def cancel_del_channel(call: CallbackQuery):
    await call.answer()
    chat_id = call.data.split('_')[1]
    text, chat_settings = await create_chat_info(chat_id)
    await call.message.edit_text(f'{text}\n\n<strong>Меню настроек чата:</strong>',
                                 reply_markup=settings_button(chat_id, chat_settings))
