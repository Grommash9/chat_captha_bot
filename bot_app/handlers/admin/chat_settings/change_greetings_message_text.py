import aiogram.types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import CallbackQuery, Message

import bot_app
from bot_app import config
from bot_app.db.chat_settings import update_greetings_message_text, get_chat_settings
from bot_app.handlers.admin.admin_echo import create_chat_info
from bot_app.keyboard.inline_markup import settings_button
from bot_app.keyboard.reply_cancel_button import cancel_btn, back_to_main_menu
from bot_app.misc import dp, bot


class NewGreetingsText(StatesGroup):
    new_text = State()


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='change-greetings-message-text')
async def edit_greetings_message_text(call: CallbackQuery, state: FSMContext):
    await call.answer()
    chat_id = call.data.split('_')[1]
    await call.message.delete()
    await bot.send_message(call.message.chat.id, 'Изменение приветственного сообщения',
                           reply_markup=cancel_btn())
    await call.message.answer('Введите новое сообщение:')
    await NewGreetingsText.new_text.set()
    await state.set_data({'chat_id': chat_id})


@dp.message_handler(text='Отмена', state=NewGreetingsText.new_text)
async def cancel_new_greetings_text(message: Message, state: FSMContext):
    data_state = await state.get_data()
    chat_id = data_state['chat_id']
    await state.finish()
    await bot.delete_message(message.from_user.id, message.message_id)
    await bot.delete_message(message.from_user.id, message.message_id - 1)
    await bot.send_message(message.from_user.id, 'Изменение приветственного сообщения отменено.',
                           reply_markup=bot_app.keyboard.reply_cancel_button.back_to_main_menu())
    text, chat_settings = await create_chat_info(chat_id)
    await bot.send_message(message.from_user.id, f'{text}\n\n<strong>Меню настроек чата:</strong>',
                           reply_markup=settings_button(chat_id, chat_settings))


@dp.message_handler(state=NewGreetingsText.new_text)
async def get_new_greetings_text(message: Message, state: FSMContext):
    new_text = message.text
    data = await state.get_data()
    chat_id = data['chat_id']
    await state.finish()
    await update_greetings_message_text(new_text=new_text, chat_id=chat_id)
    await bot.delete_message(message.from_user.id, message.message_id - 1)
    await bot.delete_message(message.from_user.id, message.message_id - 2)
    text, chat_settings = await create_chat_info(chat_id)
    await bot.send_message(message.chat.id, f'Приветственное сообщение успешно изменено',
                           reply_markup=back_to_main_menu())
    await bot.send_message(message.from_user.id, f'{text}\n\n<strong>Меню настроек чата:</strong>',
                           reply_markup=settings_button(chat_id, chat_settings))


