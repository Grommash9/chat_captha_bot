import aiogram.types
from aiogram.types import CallbackQuery, Message

import bot_app.keyboard.reply_cancel_button
from bot_app import config
from bot_app.db.global_bot_settings import get_all
from bot_app.keyboard.inline_markup import global_settings_button
from bot_app.misc import dp, bot


async def default_chat_info():
    chat_settings = await get_all()
    text = 'Текущие парамеры по умолчанию для новых чатов:\n'
    text += f'\n<i>Приветственное сообщение:</i>\n<b>{chat_settings["greetings_message_text"]}</b>\n\n' \
            f'<i>Сообщение после CAPTCHA:</i>\n<b>{chat_settings["message_after_captcha"]}</b>\n\n' \
            f'<i>Время прохождения CAPTCHA:</i>  <b>{chat_settings["time_to_pass_captcha"]}</b> сек.'
    return text


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text='Настройки для новых чатов по умолчанию')
async def choice_country(message: Message):
    await bot.send_message(message.from_user.id, 'Вы в меню настройки параметров для новых чатов',
                           reply_markup=bot_app.keyboard.reply_cancel_button.back_to_main_menu())
    settings = await get_all()
    default_settings = await default_chat_info()
    photo_link = settings['greetings_message_photo']
    await bot.send_photo(message.from_user.id, photo=photo_link, caption='Приветственное фото')
    await bot.send_message(message.from_user.id, f'{default_settings}\n\nВыберите необходимый параметр:',
                           reply_markup=global_settings_button())



