import aiogram.types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import CallbackQuery, Message

from bot_app import config
from bot_app.db.chat_settings import update_time_to_pass_captcha, get_chat_settings
from bot_app.db.global_bot_settings import update_default_time_to_pass_captcha
from bot_app.handlers.admin.admin_echo import create_chat_info
from bot_app.handlers.admin.global_settings.global_settings import default_chat_info
from bot_app.keyboard.inline_markup import settings_button, global_settings_button
from bot_app.keyboard.reply_cancel_button import cancel_btn, back_to_main_menu
from bot_app.misc import dp, bot


class SetDefaultTime(StatesGroup):
    new_time = State()


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='default-time')
async def edit_default_time(call: CallbackQuery):
    await call.answer()
    await call.message.delete()
    await bot.send_message(call.message.chat.id, 'Изменение времени для прохождения CAPTCHA',
                           reply_markup=cancel_btn())
    await call.message.answer('Введите новое время от 1 до 999:')
    await SetDefaultTime.new_time.set()


@dp.message_handler(text='Отмена', state=SetDefaultTime.new_time)
async def cancel_new_default_time(message: Message, state: FSMContext):
    await state.finish()
    await bot.delete_message(message.from_user.id, message.message_id)
    await bot.delete_message(message.from_user.id, message.message_id-1)
    await bot.send_message(message.from_user.id, 'Изменение времени отменено.',
                           reply_markup=back_to_main_menu())
    default_settings = await default_chat_info()
    await bot.send_message(message.from_user.id, f'{default_settings}\n\nВыберите необходимый параметр:',
                           reply_markup=global_settings_button())


@dp.message_handler(state=SetDefaultTime.new_time, regexp='^[1-9]$|^[1-9][0-9]$|^[1-9][0-9][0-9]$')
async def get_new_default_time(message: Message, state: FSMContext):
    await state.finish()
    new_time = message.text
    await update_default_time_to_pass_captcha(new_time)
    await bot.delete_message(message.from_user.id, message.message_id-1)
    await bot.delete_message(message.from_user.id, message.message_id-2)
    await bot.send_message(message.chat.id, f'Время для прохождения CAPTCHA успешно изменено',
                           reply_markup=back_to_main_menu())
    default_settings = await default_chat_info()
    await bot.send_message(message.from_user.id, f'{default_settings}\n\nВыберите необходимый параметр:',
                           reply_markup=global_settings_button())

