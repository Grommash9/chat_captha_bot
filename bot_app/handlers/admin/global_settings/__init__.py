from . import global_settings
from . import default_message_after_captcha
from . import default_message_text
from . import default_time_to_pass_captcha
from . import default_greetings_message_photo

