import aiogram.types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import CallbackQuery, Message

from bot_app import config
from bot_app.db.global_bot_settings import update_default_greetings_message_photo
from bot_app.handlers.admin.global_settings.global_settings import default_chat_info
from bot_app.keyboard.inline_markup import global_settings_button
from bot_app.keyboard.reply_cancel_button import cancel_btn, back_to_main_menu
from bot_app.misc import dp, bot


class NewDefaultPhotoLink(StatesGroup):
    photo_link = State()


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='default-greetings-photo')
async def edit_default_photo(call: CallbackQuery, state: FSMContext):
    await call.answer()
    await call.message.delete()
    await bot.send_message(call.message.chat.id, 'Изменение приветственного фото',
                           reply_markup=cancel_btn())
    await call.message.answer('Загрузите новое фото:')
    await NewDefaultPhotoLink.photo_link.set()


@dp.message_handler(text='Отмена', state=NewDefaultPhotoLink.photo_link)
async def cancel_new_photo(message: Message, state: FSMContext):
    await state.finish()
    await bot.delete_message(message.from_user.id, message.message_id)
    await bot.delete_message(message.from_user.id, message.message_id-1)
    await bot.send_message(message.from_user.id, 'Изменение приветственного фото отменено.',
                           reply_markup=back_to_main_menu())
    default_settings = await default_chat_info()
    await bot.send_message(message.from_user.id, f'{default_settings}\n\nВыберите необходимый параметр:',
                           reply_markup=global_settings_button())


@dp.message_handler(state=NewDefaultPhotoLink.photo_link, content_types=aiogram.types.ContentType.PHOTO)
async def get_new_default_photo(message: Message, state: FSMContext):
    photo_link = message.photo[-1].file_id
    await state.finish()
    await update_default_greetings_message_photo(photo_link)
    await bot.delete_message(message.from_user.id, message.message_id)
    await bot.delete_message(message.from_user.id, message.message_id-1)
    await bot.delete_message(message.from_user.id, message.message_id-2)
    await bot.send_message(message.chat.id, f'Приветственное фото успешно изменено',
                           reply_markup=back_to_main_menu())
    default_settings = await default_chat_info()
    await bot.send_photo(message.from_user.id, photo=photo_link, caption='Приветственное фото')
    await bot.send_message(message.from_user.id, f'{default_settings}\n\nВыберите необходимый параметр:',
                           reply_markup=global_settings_button())
