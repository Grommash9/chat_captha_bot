import aiogram.types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import CallbackQuery, Message

import bot_app
from bot_app import config
from bot_app.db.chat_settings import update_message_after_captcha, get_chat_settings
from bot_app.db.global_bot_settings import update_default_message_after_captcha
from bot_app.handlers.admin.admin_echo import create_chat_info
from bot_app.handlers.admin.global_settings.global_settings import default_chat_info
from bot_app.keyboard.inline_markup import settings_button, global_settings_button
from bot_app.keyboard.reply_cancel_button import cancel_btn, back_to_main_menu
from bot_app.misc import dp, bot


class NewDefaultGreetingsTextAfterCaptcha(StatesGroup):
    new_text = State()


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='default-text-after-captcha')
async def edit_default_text_after_captcha(call: CallbackQuery, state: FSMContext):
    await call.answer()
    await call.message.delete()
    await bot.send_message(call.message.chat.id, 'Изменение приветственного сообщения после прохождения CAPTCHA',
                           reply_markup=cancel_btn())
    await call.message.answer('Введите новое сообщение:')
    await NewDefaultGreetingsTextAfterCaptcha.new_text.set()


@dp.message_handler(text='Отмена', state=NewDefaultGreetingsTextAfterCaptcha.new_text)
async def cancel_new_greetings_text(message: Message, state: FSMContext):
    await state.finish()
    await bot.delete_message(message.from_user.id, message.message_id)
    await bot.delete_message(message.from_user.id, message.message_id-1)
    await bot.send_message(message.from_user.id, 'Изменение приветственного сообщения отменено.',
                           reply_markup=bot_app.keyboard.reply_cancel_button.back_to_main_menu())
    default_settings = await default_chat_info()
    await bot.send_message(message.from_user.id, f'{default_settings}\n\nВыберите необходимый параметр:',
                           reply_markup=global_settings_button())


@dp.message_handler(state=NewDefaultGreetingsTextAfterCaptcha.new_text)
async def get_new_greetings_text(message: Message, state: FSMContext):
    await state.finish()
    new_text = message.text
    await update_default_message_after_captcha(new_text)
    await bot.delete_message(message.from_user.id, message.message_id-1)
    await bot.delete_message(message.from_user.id, message.message_id-2)
    await bot.send_message(message.chat.id, f'Приветственное сообщение после прохождения CAPTCHA успешно изменено',
                           reply_markup=back_to_main_menu())
    default_settings = await default_chat_info()
    await bot.send_message(message.from_user.id, f'{default_settings}\n\nВыберите необходимый параметр:',
                           reply_markup=global_settings_button())

