import datetime
from zoneinfo import ZoneInfo

import aiogram.types
from aiogram.types import Message

from bot_app import config
from bot_app.handlers.chat.handler import delete_message
from bot_app.keyboard.inline_markup import show_all_statistic
from bot_app.misc import dp, bot, scheduler


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text='Статистика 📊')
async def show_stat(message: Message):

    message_ = await bot.send_message(message.from_user.id, text='Полная статистика за все время',
                                      reply_markup=show_all_statistic())
    del_time_mess = datetime.datetime.now(tz=ZoneInfo("Europe/Minsk")) + datetime.timedelta(minutes=1)
    scheduler.add_job(delete_message, 'date', run_date=del_time_mess,
                      args=(message.from_user.id, message_.message_id))
    scheduler.add_job(delete_message, 'date', run_date=del_time_mess,
                      args=(message.from_user.id, message_.message_id - 1))




