import json
import os

import bot_app
from bot_app.db.country import get_country_name
from bot_app.db.mail_log import new_log
from bot_app.db.shedul_jobs import get_job, delete_job
from bot_app.keyboard.reply_echo_admin_button import admin_echo_button
from bot_app.misc import bot


async def send_post(job_id):
    errors_log_dict = dict()
    counter_successful_shipment = 0
    text_error = ''
    admins = json.loads(os.getenv('ADMINS_ID'))
    data_send = await get_job(job_id)
    await delete_job(job_id)
    country_id = data_send[0]['country_id']
    chat_id = data_send[0]['chat_id']
    message_id = data_send[0]['message_id']
    date_send = data_send[0]['date_time']
    users = await bot_app.db.users.get_all_for_country(country_id)
    data_country = await get_country_name(country_id)
    for admin in admins:
        await bot.send_message(admin, f"Рассылка сообщений запланирована на {date_send.strftime('%H:%M %d.%m.%y')} "
                                      f"для {len(users)} пользователей запущена.",
                               reply_markup=admin_echo_button())
    for i_user in users:
        try:
            await bot.copy_message(i_user['user_id'], chat_id, message_id)
            counter_successful_shipment += 1
        except BaseException as err:
            if err in errors_log_dict.keys():
                errors_log_dict[err] = errors_log_dict.get(err) + 1
            else:
                errors_log_dict[err] = 1
    for key_error, value_error in errors_log_dict.items():
        text_error += f'{key_error} - {value_error}\n'
    if not text_error:
        text_error = '<i>- ошибок нет -</i>'
    counter_unsuccessful_shipment = sum(errors_log_dict.values())
    result_mailing = f'Рассылка сообщений завершена.\n' \
                     f'Всего разослано: {counter_successful_shipment + counter_unsuccessful_shipment} сообщений\n' \
                     f'Удачных доставок:  {counter_successful_shipment}\n' \
                     f'Неудачных доставок: {counter_unsuccessful_shipment}\n\n' \
                     f'Ошибки по неудачным доставкам:\n' \
                     f'{text_error}'
    await new_log(data_country['country_name'], date_send, len(users), counter_successful_shipment,
                  counter_unsuccessful_shipment)
    for admin in admins:
        await bot.send_message(admin, result_mailing)


