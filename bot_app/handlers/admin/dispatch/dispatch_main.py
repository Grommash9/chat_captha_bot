import datetime
import json

import aiogram
from aiogram.types import Message

from bot_app import config
import bot_app.db.users
from aiogram import types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import Dispatcher, FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.utils import executor

from bot_app.db.country import get_all_country, get_country_name
from bot_app.db.shedul_jobs import new_job
from bot_app.handlers.admin.dispatch.mass_send import send_post
from bot_app.keyboard.date_and_time.days_calendar import create_calendar
from bot_app.misc import dp, bot, scheduler

from bot_app.keyboard.reply_echo_admin_button import admin_echo_button, admin_cancel_button, admin_choice_button


class NewNotificationText(StatesGroup):
    new_notifier = State()




@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text='Создать рассылку')
async def mass_send(message: Message):
    all_country = await get_all_country()
    await bot.send_message(message.from_user.id, 'Вы в меню создания рассылки',
                           reply_markup=bot_app.keyboard.reply_cancel_button.back_to_main_menu())
    await bot.send_message(message.from_user.id, 'Выберите целевую аудиторию:',
                           reply_markup=bot_app.keyboard.reply_echo_admin_button.choice_country_dispatch(all_country))


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='Рассылка:')
async def choice_channel(message: Message, state: FSMContext):
    country_name = message.text.split(': ')[1]
    data = await bot_app.db.country.get_country_id(country_name)
    await message.answer(f'Создание рассылки для страны: {country_name}',
                         reply_markup=bot_app.keyboard.reply_cancel_button.back_to_main_menu())
    await bot.send_message(message.from_user.id, text='Введите сообщение:',
                           reply_markup=admin_cancel_button())
    await NewNotificationText.new_notifier.set()
    await state.set_data({'country_id': data['country_id']})


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                    text='Разослать', state=NewNotificationText.new_notifier)
async def mass_send(message: types.Message, state: FSMContext):
    data = await state.get_data()
    # users_dict = await bot_app.db.users.get_all()
    chat_id = data['chat_id']
    message_id = data['message_id']
    country_id = data['country_id']
    date_data = data['date']
    (_, year, month, day, hour) = date_data
    # date_send = datetime.datetime(int(year), int(month), int(day), int(hour))
    date_send = datetime.datetime.now() + datetime.timedelta(seconds=10)
    users = await bot_app.db.users.get_all_for_country(country_id)
    country_data = await get_country_name(country_id)
    await state.finish()
    job_id = await new_job(country_id, chat_id, message_id, date_send)
    scheduler.add_job(send_post, 'date', run_date=date_send, args=[job_id], id=str(job_id))
    await bot.send_message(message.from_user.id,
                           f"Запланирована рассылка сообщений:\n"
                           f"- <i>целевая страна:</i> <b>{country_data['country_name']}\n</b>"
                           f"- <i>количество пользователей:</i> <b>{len(users)}</b>\n"
                           f"- <i>время начала:</i> <b>{date_send.strftime('%H:%M %d.%m.%y')}</b>",
                           reply_markup=admin_echo_button())


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                    text='Отмена', state=NewNotificationText.new_notifier)
async def mass_send(message: types.Message, state: FSMContext):
    await state.finish()
    await bot.delete_message(message.from_user.id, message.message_id)
    await bot.delete_message(message.from_user.id, message.message_id-1)
    await bot.send_message(message.from_user.id, text='Создание рассылки отменено.',
                           reply_markup=admin_echo_button())


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), content_types=aiogram.types.ContentType.ANY,
                    state=NewNotificationText.new_notifier)
async def mass_send(message: types.Message, state: FSMContext):
    await state.update_data({'message_id': message.message_id, 'chat_id': message.chat.id})
    await bot.send_message(message.from_user.id, 'Сообщение готово к отправке. \nВыберите дату и время начала рассылки:',
                           reply_markup=create_calendar())



