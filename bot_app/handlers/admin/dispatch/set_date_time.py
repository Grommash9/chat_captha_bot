import json
import datetime

import aiogram
from aiogram.dispatcher import FSMContext
from aiogram.types import ReplyKeyboardRemove, CallbackQuery
from bot_app.handlers.admin.dispatch.dispatch_main import NewNotificationText

from bot_app import config
from bot_app.keyboard.date_and_time.days_calendar import create_calendar
from bot_app.keyboard.date_and_time.free_hours import free_time
from bot_app.keyboard.reply_echo_admin_button import admin_choice_button
from bot_app.misc import bot, dp


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='back-to-date',
                           state=NewNotificationText.new_notifier)
async def back_to_date(call: CallbackQuery, state: FSMContext):
    await call.answer()
    now = datetime.datetime.now()
    await call.message.edit_text('Выберите день:',
                                 reply_markup=create_calendar(year=now.year, month=now.month))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='time_',
                           state=NewNotificationText.new_notifier)
async def set_time_order(call: CallbackQuery, state: FSMContext):
    await call.answer()
    await call.message.delete()
    data = call.data.split('_')
    (_, year, month, day, hour) = data
    await state.update_data({'date': data})
    await bot.send_message(call.from_user.id,
                           f'Создание рассылки успешно завершено.\n'
                           f'Пожалуйста, выберите действие:',
                           reply_markup=admin_choice_button())


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text='ignore', state='*')
async def ignore_button(call: CallbackQuery):
    await call.answer('Пожалуйста, выберите дату со значком "⚡" или "🔹"', True)


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='day',
                           state=NewNotificationText.new_notifier)
async def sey_day(call: CallbackQuery, state: FSMContext):
    data = call.data.split('_')
    (_, year, month, day) = data
    await call.message.delete_reply_markup()
    await call.message.answer(f'Выбрана дата:   {int(day):02d}.{int(month):02d}.{year}')
    await call.message.answer('Выберите время: ',
                              reply_markup=free_time(year, month, day))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='prev-month',
                               state=NewNotificationText.new_notifier)
async def inline11(call: CallbackQuery, state: FSMContext):
    data = call.data.split('_')
    (_, year, month, day) = data
    prev_date = datetime.datetime(int(year), int(month), 1) - datetime.timedelta(days=1)
    await call.message.edit_reply_markup(reply_markup=create_calendar(year=int(prev_date.year),
                                                                      month=int(prev_date.month)))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='next-month',
                           state=NewNotificationText.new_notifier)
async def inline11(call: CallbackQuery, state: FSMContext):
    data = call.data.split('_')
    (_, year, month, day) = data
    next_mouth = datetime.datetime(int(year), int(month), 1) + datetime.timedelta(days=31)
    await call.message.edit_reply_markup(reply_markup=create_calendar(year=int(next_mouth.year),
                                                                      month=int(next_mouth.month)))