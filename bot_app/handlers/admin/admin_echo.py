import aiogram.types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import state
from aiogram.types import CallbackQuery, Message
from aiogram.utils.exceptions import ChatNotFound, BotKicked, MessageToDeleteNotFound

import bot_app.keyboard.reply_cancel_button
from bot_app import config
from bot_app.db.chat_settings import get_all_chat_id, get_chat_settings, get_all_chat_id_for_country, \
    get_target_channels
from bot_app.db.country import get_all_country
from bot_app.keyboard.inline_markup import settings_button
from bot_app.keyboard.inline_markup import all_chat_id
from bot_app.misc import dp, bot, redis
from bot_app.keyboard.reply_echo_admin_button import admin_echo_button, admin_cancel_button


async def create_chat_info(chat_id):
    data = await bot.get_chat(chat_id)
    chat_name = data['title']
    list_channels = await get_target_channels(chat_id)
    text = f'Выбран чат:  <strong> {chat_name}</strong>\n\n<i>Целевые каналы для выбранного чата:</i>\n' \
           f'----------------------\n'
    for i_channel in list_channels:
        channel_id = i_channel['channel_id']
        if channel_id:
            try:
                channel_info = await bot.get_chat(channel_id)
            except ChatNotFound:
                pass
            else:
                text += f"{channel_info['title']}   {i_channel['url']}\n" \
                        f"----------------------\n"
    chat_settings = await get_chat_settings(int(chat_id))
    text += f'\n<i>Приветственное сообщение:</i>\n<b>{chat_settings["greetings_message_text"]}</b>\n\n' \
            f'<i>Сообщение после CAPTCHA:</i>\n<b>{chat_settings["message_after_captcha"]}</b>\n\n' \
            f'<i>Время прохождения CAPTCHA:</i>  <b>{chat_settings["time_to_pass_captcha"]}</b> сек.'
    return text, chat_settings


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), commands=['admin', 'start'], state='*')
async def admin_echo(message: Message, state: FSMContext):
    await state.finish()
    await bot.send_message(chat_id=message.from_user.id, text=f'Приветствую {message.from_user.username}',
                           reply_markup=admin_echo_button())


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text='Настройка чатов ⚙')
async def choice_country(message: Message):
    all_country = await get_all_country()
    await bot.send_message(message.from_user.id, 'Вы в меню настройки чатов!',
                           reply_markup=bot_app.keyboard.reply_cancel_button.back_to_main_menu())
    await bot.send_message(message.from_user.id, 'Выберите целевую страну:',
                           reply_markup=bot_app.keyboard.reply_echo_admin_button.change_country_button(all_country))


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text='вернутся в основное меню')
async def back_to_main_menu(message: Message):
    try:
        await bot.delete_message(message.from_user.id, message.message_id-1)
        await bot.delete_message(message.from_user.id, message.message_id-2)
    except MessageToDeleteNotFound:
        pass
    await bot.send_message(message.from_user.id, 'Возврат в основное меню',
                           reply_markup=admin_echo_button())


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text='Польша')
async def choice_channel(message: Message):
    await message.answer('Страна: <b>Польша</b>', reply_markup=bot_app.keyboard.reply_cancel_button.back_to_main_menu())
    all_chat_dict = await get_all_chat_id_for_country(1)
    chat_data_list = []
    for chat_id in all_chat_dict:
        try:
            chat_data = await bot.get_chat(chat_id['chat_id'])
            chat_data_list.append(chat_data)
        except (ChatNotFound, BotKicked):
            await bot.send_message(chat_id=message.from_user.id,
                                   text=f'Внимание! В одном из чатов бот не является админом!')
    await bot.send_message(chat_id=message.from_user.id, text='Доступные чаты для настройки:',
                           reply_markup=all_chat_id(chat_data_list, country_id=1))


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text='Испания')
async def choice_channel(message: Message):
    await message.answer('Страна: <b>Испания</b>', reply_markup=bot_app.keyboard.reply_cancel_button.back_to_main_menu())
    all_chat_dict = await get_all_chat_id_for_country(2)
    chat_data_list = []
    for chat_id in all_chat_dict:
        try:
            chat_data = await bot.get_chat(chat_id['chat_id'])
            chat_data_list.append(chat_data)
        except (ChatNotFound, BotKicked):
            await bot.send_message(chat_id=message.from_user.id,
                                   text=f'Внимание! В одном из чатов бот не является админом!')
    await bot.send_message(chat_id=message.from_user.id, text='Доступные чаты для настройки:',
                           reply_markup=all_chat_id(chat_data_list, country_id=2))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='edit-channel')
async def settings_menu(call: CallbackQuery):
    await call.answer()
    await call.message.delete()
    chat_id = int(call.data.split('_')[1])
    text, chat_settings = await create_chat_info(chat_id)
    await call.message.answer(f'{text}\n\n<strong>Меню настроек чата:</strong>',
                              reply_markup=settings_button(chat_id, chat_settings))


# @dp.message_handler(aiogram.filters.ChatTypeFilter(aiogram.types.ChatType.PRIVATE))
# async def eny_text_messages(message: Message):
#     if message.from_user.id not in config.ADMINS_ID:
#         await bot.send_message(chat_id=message.from_user.id, text='Вы не являетесь админом данного бота!')
#         return
#
#     await bot.send_message(chat_id=message.from_user.id,
#                            text='Ошибочный ввод. Возможно вы имели ввиду: /start или /admin')

