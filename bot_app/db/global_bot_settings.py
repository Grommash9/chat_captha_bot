from bot_app.db.base import create_dict_con


async def get_all() -> dict:
    """
    Получение глобальных настроек бота
    :param param:
    :return: словарь где ключи это названия параметров, а значения их значения пример:
    {'greetings_message_photo': None,
     'greetings_message_text': 'привет пройдите капчу',
      'message_after_captcha': 'вы прошли капчу в чат такой-то спасибо',
       'target_channel_id': '-1001783944910',
        'target_channel_url': 'https://t.me/+4Xp5a7ZpwHg1NjZi'}

    """
    con, cur = await create_dict_con()
    await cur.execute('select * from global_bot_settings ')
    settings = await cur.fetchall()
    settings_dict = dict()
    for data in settings:
        settings_dict[data['param']] = data['value']
    await con.ensure_closed()
    return settings_dict


async def get(param) -> dict:
    """
    Получение определенного параметра из глобальных настроек
    :param param: название параметра
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('select * from global_bot_settings where param = %s ', (param,))
    setting_param = await cur.fetchone()
    await con.ensure_closed()
    return setting_param


async def update_default_greetings_message_text(text: str):
    """
    установка нового значения приветственного сообщения
    :param text: значение
    """
    con, cur = await create_dict_con()
    await cur.execute('delete from global_bot_settings where param = "greetings_message_text"')
    await cur.execute('insert ignore into global_bot_settings (param, value) values (%s, %s) ',
                      ("greetings_message_text", text))
    await con.commit()
    await con.ensure_closed()


async def update_default_message_after_captcha(text: str):
    """
    установка нового значения приветственного сообщения после проверки
    :param text: значение
    """
    con, cur = await create_dict_con()
    await cur.execute('delete from global_bot_settings where param = "message_after_captcha"')
    await cur.execute('insert ignore into global_bot_settings (param, value) values (%s, %s) ',
                      ("message_after_captcha", text))
    await con.commit()
    await con.ensure_closed()


async def update_default_time_to_pass_captcha(num: int):
    """
    установка нового значения времени на проверку
    :param num: значение
    """
    con, cur = await create_dict_con()
    await cur.execute('delete from global_bot_settings where param = "time_to_pass_captcha"')
    await cur.execute('insert ignore into global_bot_settings (param, value) values (%s, %s) ',
                      ("time_to_pass_captcha", num))
    await con.commit()
    await con.ensure_closed()


async def update_default_greetings_message_photo(url: str):
    """
    установка нового значения времени на проверку
    :param url: значение
    """
    con, cur = await create_dict_con()
    await cur.execute('delete from global_bot_settings where param = "greetings_message_photo"')
    await cur.execute('insert ignore into global_bot_settings (param, value) values (%s, %s) ',
                      ("greetings_message_photo", url))
    await con.commit()
    await con.ensure_closed()
