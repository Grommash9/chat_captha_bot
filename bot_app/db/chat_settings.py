from pymysql import IntegrityError

import bot_app.db.global_bot_settings
from bot_app.db.base import create_dict_con, create_con


async def create_chat(country_id: int, chat_id: int) -> None:
    """
    Создать запись о новом чате в базу данных, что бы потом можно было его настраивать
    :param chat_id: айди нового чата
    :return:
    """
    con, cur = await create_dict_con()
    global_bot_settings = await bot_app.db.global_bot_settings.get_all()
    try:

        await cur.execute('insert into chat_settings (chat_id, country_id, '
                          'greetings_message_text, greetings_message_photo, message_after_captcha, time_to_pass_captcha) '
                          'values (%s, %s, %s, %s, %s, %s)',
                          (chat_id, country_id, global_bot_settings['greetings_message_text'],
                           global_bot_settings['greetings_message_photo'],
                           global_bot_settings['message_after_captcha'],
                           global_bot_settings['time_to_pass_captcha']))
    except IntegrityError:
        return
    await cur.execute('insert ignore into chat_channels (chat_id) values (%s)', (chat_id,))
    await con.commit()
    await con.ensure_closed()


async def get_chat_settings(chat_id: int) -> dict:
    """
    Получить основные настройки чата
    :param chat_id: айди чата для которого хотим получить настройки
    :return: словарь с настройками чата
    """
    con, cur = await create_dict_con()
    await cur.execute('select * from chat_settings where chat_id = %s ', (chat_id,))
    chat_data = await cur.fetchone()
    await con.ensure_closed()
    return chat_data


async def get_all_chat_settings() -> dict:
    """
    Получить настройки всех чатов
    :return: словарь с настройками чата
    """
    con, cur = await create_dict_con()
    await cur.execute('select * from chat_settings')
    chat_data = await cur.fetchall()
    await con.ensure_closed()
    return chat_data


async def get_target_channels(chat_id: int) -> list[dict]:
    """
    Получить каналы на которые нужно подписаться для получения возможности писать в чате
    :param chat_id: айди целевого чата
    :return: список словарей с целевыми каналами в виде:
    [{'channel_id': -1001783944910, 'url': https://t.me/+Lr6ThJV2-u4yZjIy},
     {'channel_id': -1001211251574, 'url': 'https://t.me/+Lr6ThJV2-uF4dfjIy'}]]
    """
    con, cur = await create_dict_con()
    await cur.execute('select channel_id, url from chat_channels where chat_id = %s ', (chat_id,))
    chat_data = await cur.fetchall()
    await con.ensure_closed()
    return chat_data


async def get_all_chat_id():
    con, cur = await create_dict_con()
    await cur.execute('select chat_id from chat_settings')
    chat_data = await cur.fetchall()
    await con.ensure_closed()
    return chat_data


async def get_all_chat_id_for_country(country_id):
    con, cur = await create_dict_con()
    await cur.execute('select chat_id from chat_settings where country_id =  %s', (country_id, ))
    data_country = await cur.fetchall()
    await con.ensure_closed()
    return data_country


async def add_target_channel(chat_id: int, channel_id: int, channel_url: str) -> None:
    """
    Добавиление канала в список каналов для обязательной подписки что бы писать в чате
    :param chat_id: айди чата для которого добавляем новый целевой канал
    :param channel_id: айди нового целевого канала
    :param channel_url: ссылка на новый целевой канал
    """
    con, cur = await create_dict_con()
    await cur.execute('insert ignore into chat_channels (chat_id, channel_id, url) values (%s, %s, %s) ',
                      (chat_id, channel_id, channel_url))
    await con.commit()
    await con.ensure_closed()


async def delete_target_channel(chat_id: int, channel_id: int) -> None:
    """
    Удаление канала из списка каналов для обязательной подписки
    :param chat_id: айди чата для которого удаляем целевой канал
    :param channel_id:  айди удаляемого целевого канала
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('delete from chat_channels where chat_id = %s and channel_id = %s ',
                      (chat_id, channel_id,))
    await con.commit()
    await con.ensure_closed()


async def update_greetings_message_text(new_text: str, chat_id: int):
    """
    Установка нового приветственного сообщения
    :param new_text: новое сообщение
    :param chat_id: чат который настраиваем
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('update chat_settings set greetings_message_text = %s where chat_id = %s', (new_text, chat_id, ))
    await con.commit()
    await con.ensure_closed()


async def update_greetings_message_photo(photo_id: str, chat_id: int):
    """
    Установка нового фото приветственного сообщения
    :param new_text: новое фото айди для сообщения
    :param chat_id: чат который настраиваем
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('update chat_settings set greetings_message_photo = %s where chat_id = %s', (photo_id, chat_id, ))
    await con.commit()
    await con.ensure_closed()


async def switch_bot_enabled(chat_id: int):
    """
    Переключить бота вкл/выкл
    :param chat_id: чат который настраиваем
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('update chat_settings set bot_enabled = not bot_enabled where chat_id = %s', (chat_id, ))
    await con.commit()
    await con.ensure_closed()


async def switch_bot_enabled_for_country(country_id: int):
    """
    Переключить бота вкл/выкл для всех чатов по стране
    :param country_id: страна по которой настраиваем
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('update chat_settings set bot_enabled = not bot_enabled where country_id = %s', (country_id, ))
    await con.commit()
    await con.ensure_closed()

async def update_message_after_captcha(new_text: str, chat_id: int):
    """
    Установка нового сообщения после прохождения капчи
    :param new_text: новое сообщение
    :param chat_id: чат который настраиваем
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('update chat_settings set message_after_captcha = %s where chat_id = %s', (new_text, chat_id, ))
    await con.commit()
    await con.ensure_closed()


async def update_time_to_pass_captcha(time: int, chat_id: int):
    """
    Установка нового времени для прохождения капчи
    :param time: время в секундах
    :param chat_id: чат который настраиваем
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('update chat_settings set time_to_pass_captcha = %s where chat_id = %s', (time, chat_id, ))
    await con.commit()
    await con.ensure_closed()


async def switch_kick_from_chat_on_fail(chat_id: int):
    """
    вкл/выкл кикать из чата за не прохождение капчи
    :param chat_id: чат который настраиваем
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('update chat_settings set kick_from_chat_on_fail = not kick_from_chat_on_fail '
                      'where chat_id = %s', (chat_id, ))
    await con.commit()
    await con.ensure_closed()


async def switch_ban_from_chat_on_fail(chat_id: int):
    """
    вкл/выкл банить в чате за не прохождение капчи
    :param chat_id: чат который настраиваем
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('update chat_settings set ban_from_chat_on_fail = not ban_from_chat_on_fail '
                      'where chat_id = %s', (chat_id, ))
    await con.commit()
    await con.ensure_closed()


async def switch_skip_verified_users(chat_id: int):
    """
    :param chat_id: чат который настраиваем
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('update chat_settings set skipVerifiedUsers = not skipVerifiedUsers '
                      'where chat_id = %s', (chat_id, ))
    await con.commit()
    await con.ensure_closed()
