from pymysql import IntegrityError

import bot_app.db.global_bot_settings
from bot_app.db.base import create_dict_con, create_con


async def get_all_country() -> dict:
    """
    Получить все страны
    """
    con, cur = await create_dict_con()
    await cur.execute('select * from country')
    country_data = await cur.fetchall()
    await con.ensure_closed()
    return country_data


async def get_country_id(country_name) -> dict:
    """
    Получить id страны по названию
    """
    con, cur = await create_dict_con()
    await cur.execute('select country_id from country where country_name = %s', (country_name, ))
    country_data = await cur.fetchone()
    await con.ensure_closed()
    return country_data


async def get_country_name(country_id) -> dict:
    """
    Получить название страны по id
    """
    con, cur = await create_dict_con()
    await cur.execute('select country_name from country where country_id = %s', (country_id, ))
    country_data = await cur.fetchone()
    await con.ensure_closed()
    return country_data
