from pymysql import IntegrityError

import bot_app.db.global_bot_settings
from bot_app.db.base import create_dict_con, create_con


async def bot_disabled_for_country(country_id: int):
    """
    Выключить бота для всех чатов по стране
    :param country_id: страна
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('update chat_settings set bot_enabled = 0 where country_id = %s', (country_id, ))
    await con.commit()
    await con.ensure_closed()


async def bot_enabled_for_country(country_id: int):
    """
    Включить бота для всех чатов по стране
    :param country_id: страна
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('update chat_settings set bot_enabled = 1 where country_id = %s', (country_id, ))
    await con.commit()
    await con.ensure_closed()