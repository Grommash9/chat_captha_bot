from typing import Optional

from bot_app.db.base import create_dict_con
from aiogram.types import User


async def create_user(user: User) -> dict:
    con, cur = await create_dict_con()
    await cur.execute('insert ignore into users (telegram_id, user_name) '
                      'values (%s, %s)',
                      (user.id, user.username))
    await con.commit()
    await cur.execute('select * from users where telegram_id = %s ', (user.id,))
    user_data = await cur.fetchone()
    await con.ensure_closed()
    return user_data


async def get_user(user_id: int) -> Optional[dict]:
    con, cur = await create_dict_con()
    await cur.execute('select * from users where telegram_id = %s ', (user_id,))
    user_data = await cur.fetchone()
    await con.ensure_closed()
    return user_data


async def get_all() -> list[dict]:
    """
    Получить список всех пользователей
    :return: список словарей с данными пользователей
    """
    con, cur = await create_dict_con()
    await cur.execute('select * from users ')
    users = await cur.fetchall()
    await con.ensure_closed()
    return users


async def get_all_for_country(country_id) -> list[dict]:
    """
    Получить список всех пользователей по стране
    :return: список словарей с данными пользователей
    """
    con, cur = await create_dict_con()
    await cur.execute('select telegram_id as user_id from users join chats_users on users.telegram_id = chats_users.user_id '
                      'join chat_channels on chats_users.chat_id = chat_channels.chat_id '
                      'join chat_settings on chats_users.chat_id = chat_settings.chat_id where country_id = %s '
                      'group by telegram_id', (country_id, ))
    users = await cur.fetchall()
    await con.ensure_closed()
    return users
