from bot_app.db.base import create_dict_con


async def new_note_members(channel_id, channel_name, date, member=0, left=0):
    """
    Создать новую запись о изменениях в подписчиках каналов
    """
    con, cur = await create_dict_con()
    await cur.execute('select country_name from country '
                      'join chat_settings on chat_settings.country_id = country.country_id '
                      'join chat_channels on chat_channels.chat_id = chat_settings.chat_id '
                      'where chat_channels.channel_id  =  %s', (channel_id,))
    sql = await cur.fetchone()
    await cur.execute('insert into members (date, country_name, channel_name, member_user, left_user) '
                      'values (%s, %s, %s, %s, %s)', (date, sql["country_name"], channel_name, member, left))
    await con.commit()
    await con.ensure_closed()