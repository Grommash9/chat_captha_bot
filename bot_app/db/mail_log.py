from bot_app.db.base import create_dict_con


async def new_log(country_name, date_time, count_users, successful_delivery, unsuccessful_delivery):
    """
    Создать новую запись о результатах рассылки
    """
    con, cur = await create_dict_con()
    await cur.execute('insert into mail_log (country_name, date_time, count_users, sucsesfull_delivery, unsucsesfull_delivery) '
                      'values (%s, %s, %s, %s, %s)',
                      (country_name, date_time, count_users, successful_delivery, unsuccessful_delivery))
    await con.commit()
    await con.ensure_closed()
