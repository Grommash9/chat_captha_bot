from bot_app.db.base import create_dict_con


async def new_note(chat_id, date, start_check=0, member=0, ban=0, kicked=0):
    """
    Создать новую запись о результатах проверки
    """
    con, cur = await create_dict_con()
    await cur.execute('select country.country_name from country '
                      'join chat_settings on chat_settings.country_id = country.country_id '
                      'where chat_settings.chat_id  =  %s', (chat_id,))
    sql = await cur.fetchone()
    await cur.execute('insert into captcha_log (country_name, kicked, ban, date, start_check, member) '
                      'values (%s, %s, %s, %s, %s, %s)', (sql["country_name"], kicked, ban, date, start_check, member))
    await con.commit()
    await con.ensure_closed()


