from typing import Optional

from bot_app.db.base import create_dict_con


async def create_chat_user(chat_id: int, user_id: int) -> None:
    """
    Добавляем пользователя как авторизованного для того что бы писать в нашем чате после того как он прошел проверку
    или даже после того как ему не потребовалось проходить проверку, будем использовать возмонжо для того что бы связать
    потом пользователей из таблицы users с чатами в которых они учавствуют для целевой рассылки
    :param chat_id: чат в котором пользователь прошел проверку
    :param user_id: айди пользователя прошедшего проверку
    :return:
    """
    con, cur = await create_dict_con()

    await cur.execute('insert ignore into chats_users (chat_id, user_id ) '
                      'values (%s, %s)',
                      (chat_id, user_id))
    await con.commit()
    await con.ensure_closed()


async def get_users_chats(user_id: int) -> Optional[list[dict]]:
    """
    Получить список чатов в которых пользователь был авторизован
    :param user_id: интересующий нас пользователь
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('select chat_id from chats_users where user_id = %s ', (user_id, ))
    user_chat_data = await cur.fetchall()
    await con.ensure_closed()
    return user_chat_data
