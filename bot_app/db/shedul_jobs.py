from bot_app.db.base import create_dict_con


async def new_job(country_id, chat_id, message_id, date_time) -> int:
    """
    Создать новый занятый слот рабочего и получить его айди для использования в создании заявки
    :param worker_id: айди работника
    :param date_time: дата резервирования времени на заявку
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('insert into shedul_jobs (country_id, chat_id, message_id, date_time) '
                      'values (%s, %s, %s, %s)',
                      (country_id, chat_id, message_id, date_time))
    await con.commit()
    await con.ensure_closed()
    return cur.lastrowid


async def get_job(job_id):
    con, cur = await create_dict_con()
    await cur.execute('select * from shedul_jobs where job_id = %s', (job_id,))
    data = await cur.fetchall()
    await con.ensure_closed()
    return data


async def delete_job(job_id):
    con, cur = await create_dict_con()
    await cur.execute('delete from shedul_jobs where job_id = %s', (job_id,))
    await con.commit()
    await con.ensure_closed()

