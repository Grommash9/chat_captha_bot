import json
from pathlib import Path

from dotenv import load_dotenv
cwd = Path().cwd()

load_dotenv()
import os


ADMINS_ID = json.loads(os.getenv('ADMINS_ID'))
BOT_TOKEN = os.getenv('BOT_TOKEN')

REDIS = {
    'db': 2,
    'prefix': cwd.name
}

MYSQL = {
    'host': os.getenv('MYSQL_HOST'),
    'user': os.getenv('MYSQL_USER'),
    'password': os.getenv('MYSQL_PASSWORD'),
    'db': os.getenv('MYSQL_DB_NAME'),
    # 'unix_socket': '/var/run/mysqld/mysqld.sock'
}
