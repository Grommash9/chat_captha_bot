-- MySQL dump 10.13  Distrib 8.0.30, for Linux (x86_64)
--
-- Host: localhost    Database: chat_captha_bot_db
-- ------------------------------------------------------
-- Server version	8.0.30-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chat_channels`
--

DROP TABLE IF EXISTS `chat_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chat_channels` (
  `chat_id` bigint DEFAULT NULL,
  `channel_id` bigint DEFAULT NULL,
  `url` varchar(2000) DEFAULT NULL,
  UNIQUE KEY `chat_channels_chat_id_channel_id_uindex` (`chat_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_channels`
--

LOCK TABLES `chat_channels` WRITE;
/*!40000 ALTER TABLE `chat_channels` DISABLE KEYS */;
INSERT INTO `chat_channels` VALUES (-1001732796780,-1001716507714,'https://t.me/+8Udj5DK_ImcxYTcy'),(-1001732796780,-1001549254765,'https://t.me/+DIMzyg3jM4A2Mjgy');
/*!40000 ALTER TABLE `chat_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_settings`
--

DROP TABLE IF EXISTS `chat_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chat_settings` (
  `chat_id` bigint NOT NULL,
  `greetings_message_text` varchar(2000) DEFAULT NULL,
  `greetings_message_photo` varchar(2000) DEFAULT NULL,
  `bot_enabled` int DEFAULT '1',
  `message_after_captcha` varchar(2000) DEFAULT NULL,
  `time_to_pass_captcha` int DEFAULT '60',
  `kick_from_chat_on_fail` int DEFAULT '1',
  `ban_from_chat_on_fail` int DEFAULT '0',
  `skipVerifiedUsers` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`chat_id`),
  UNIQUE KEY `chat_settings_chat_id_uindex` (`chat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_settings`
--

LOCK TABLES `chat_settings` WRITE;
/*!40000 ALTER TABLE `chat_settings` DISABLE KEYS */;
INSERT INTO `chat_settings` VALUES (-1001732796780,'привет пройдите капчу',NULL,1,'вы прошли капчу в чат такой-то спасибо',60,1,0,0);
/*!40000 ALTER TABLE `chat_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chats_users`
--

DROP TABLE IF EXISTS `chats_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chats_users` (
  `chat_id` bigint DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  UNIQUE KEY `chats_users_chat_id_user_id_uindex` (`chat_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chats_users`
--

LOCK TABLES `chats_users` WRITE;
/*!40000 ALTER TABLE `chats_users` DISABLE KEYS */;
INSERT INTO `chats_users` VALUES (-1001549254765,1278563327),(-1001549254765,5271842643);
/*!40000 ALTER TABLE `chats_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `global_bot_settings`
--

DROP TABLE IF EXISTS `global_bot_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `global_bot_settings` (
  `param` varchar(200) NOT NULL,
  `value` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`param`),
  UNIQUE KEY `global_bot_settings_param_uindex` (`param`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `global_bot_settings`
--

LOCK TABLES `global_bot_settings` WRITE;
/*!40000 ALTER TABLE `global_bot_settings` DISABLE KEYS */;
INSERT INTO `global_bot_settings` VALUES ('greetings_message_photo',NULL),('greetings_message_text','привет пройдите капчу'),('message_after_captcha','вы прошли капчу в чат такой-то спасибо'),('target_channel_id','-1001716507714'),('target_channel_url','https://t.me/+8Udj5DK_ImcxYTcy');
/*!40000 ALTER TABLE `global_bot_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `telegram_id` bigint NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`telegram_id`),
  UNIQUE KEY `users_telegram_id_uindex` (`telegram_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5511588440 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1278563327,'porzadna_reklama'),(5271842643,'valentina_test8');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-15  6:44:56
